using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace UdpTimeServer
{
    class Program
    {
        private static Socket _udpSocket;
        private static IPAddress _addr;
        private const int _port = 65535;
        private static EndPoint _localEP;
        private static EndPoint _remoteEP;
        private static byte[] _data;

        static void Main(string[] args)
        {
            _addr = IPAddress.Parse("127.0.0.1");
            _localEP = new IPEndPoint(_addr, _port);
            _remoteEP = new IPEndPoint(IPAddress.Any, 0);
            _data = new byte[256]; // buffer
            _udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.IP);
            _udpSocket.Bind(_localEP);
            Console.WriteLine("Udp time sender started!");
            Console.WriteLine("Waiting for request...");
            while (true)
            {
                int bytes = _udpSocket.ReceiveFrom(_data, ref _remoteEP);
                string txtReq = Encoding.UTF8.GetString(_data, 0, bytes).Trim();
                Console.WriteLine("Command from {1}:{2}: {0}", txtReq, (_remoteEP as IPEndPoint).Address, (_remoteEP as IPEndPoint).Port);
                if (txtReq.Equals("time"))
                {
                    Console.WriteLine("Send time to {0}:{1}", (_remoteEP as IPEndPoint).Address, (_remoteEP as IPEndPoint).Port);
                    byte[] bTime = Encoding.UTF8.GetBytes(DateTime.Now.ToString("HH:mm:ss"));
                    _udpSocket.SendTo(bTime, _remoteEP);
                }
            }
        }
    }
}
