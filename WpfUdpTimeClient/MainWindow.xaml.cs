﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Net;
using System.Net.Sockets;


namespace WpfUdpTimeClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer timer;
        private Socket _udpSocket;
        private IPAddress _addr;
        private const int _port = 65532;
        private const int _rport = 65535;
        private EndPoint _localEP;
        private EndPoint _remoteEP;
        private byte[] _data;

        public MainWindow()
        {
            InitializeComponent();
            
            _addr = IPAddress.Parse("127.0.0.1");
            _localEP = new IPEndPoint(_addr, _port);
            _remoteEP = new IPEndPoint(_addr, _rport);
            _data = new byte[256]; // buffer
            _udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.IP);
            _udpSocket.Bind(_localEP);
            timer = new DispatcherTimer();
            timer.Tick += timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            byte[] bTime = Encoding.UTF8.GetBytes("time");
            _udpSocket.SendTo(bTime, _remoteEP);

            int bytes = _udpSocket.ReceiveFrom(_data, ref _remoteEP);
            string txtReq = Encoding.UTF8.GetString(_data, 0, bytes).Trim();
            labelTime.Content = txtReq;
        }
    }
}
