﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace FormsUDPTimeClient
{
    public partial class MainForm : Form
    {
        private static Socket _udpSocket;
        private static IPAddress _addr;
        private const int _port = 65533;
        private const int _rport = 65535;
        private static EndPoint _localEP;
        private static EndPoint _remoteEP;
        private static byte[] _data;

        public MainForm()
        {
            InitializeComponent();
            _addr = IPAddress.Parse("127.0.0.1");
            _localEP = new IPEndPoint(_addr, _port);
            _remoteEP = new IPEndPoint(_addr, _rport);
            _data = new byte[256]; // buffer
            _udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.IP);
            _udpSocket.Bind(_localEP);
        }

        private void timerGetTime_Tick(object sender, EventArgs e)
        {
            byte[] bTime = Encoding.UTF8.GetBytes("time");
            _udpSocket.SendTo(bTime, _remoteEP);

            int bytes = _udpSocket.ReceiveFrom(_data, ref _remoteEP);
            string txtReq = Encoding.UTF8.GetString(_data, 0, bytes).Trim();
            labelTime.Text = txtReq;
        }
    }
}
